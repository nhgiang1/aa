import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { DynamicFormComponent } from './dynamic-form/dynamic-form.component';
import { InputControlComponent } from './dynamic-form/component/input-control/input-control.component';
import { DateControlComponent } from './dynamic-form/component/date-control/date-control.component';
import { DynamicFormDirective } from './dynamic-form/dynamic-form.directive';
import {ReactiveFormsModule} from "@angular/forms";
import { SelectControlComponent } from './dynamic-form/component/select-control/select-control.component';

@NgModule({
  declarations: [
    AppComponent,
    DynamicFormComponent,
    InputControlComponent,
    DateControlComponent,
    DynamicFormDirective,
    SelectControlComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [
    InputControlComponent,
    DateControlComponent,
    SelectControlComponent
  ],
})
export class AppModule { }
