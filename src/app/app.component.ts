import {Component, ViewChild} from '@angular/core';
import {Field} from './dynamic-form/model/field';
import {DynamicFormComponent} from "./dynamic-form/dynamic-form.component";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent{
  constructor(){}
  @ViewChild('appDynamicForm',{static:true}) appDynamicForm: DynamicFormComponent;
  fields: Field[] = [
    {
      type: 'input',
      label: 'Loại sự vụ',
      inputType: 'text',
      name: 'VOC',
    },
    {
      type: 'date',
      label: 'Ngày bắt đầu',
      name: 'date',
    },
    {
      type: 'select',
      label: 'Tình trạng',
      name: 'status',
      value: 'active',
      options: ['active','noActive'],
    },
  ];

  submit() {
    console.log(this.appDynamicForm.value);
  }
}
