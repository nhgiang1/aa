import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Field} from './model/field';
import {FormBuilder, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-dynamic-form',
  templateUrl: './dynamic-form.component.html',
  styleUrls: ['./dynamic-form.component.scss']
})
export class DynamicFormComponent implements OnInit {
  @Input() fields: Field[] = [];
  form: FormGroup;
  @Output() submit: EventEmitter<any> = new EventEmitter<any>();

  get value() {
    return this.form.value;
  }

  constructor(private fb: FormBuilder) {
  }

  ngOnInit() {
    this.form = this.createControl();
  }

  createControl() {
    const group = this.fb.group({});
    this.fields.forEach(field => {
      const conntrol = this.fb.control(field.value);
      group.addControl(field.name, conntrol);
    });
    return group;
  }
}
