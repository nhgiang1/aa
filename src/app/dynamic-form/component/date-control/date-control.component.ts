import { Component, OnInit } from '@angular/core';
import {FormGroup} from "@angular/forms";
import {Field} from "../../model/field";

@Component({
  selector: 'app-date-control',
  templateUrl: './date-control.component.html',
  styleUrls: ['./date-control.component.scss']
})
export class DateControlComponent implements OnInit {
  group: FormGroup;
  field: any;
  constructor() { }

  ngOnInit() {
  }

}
