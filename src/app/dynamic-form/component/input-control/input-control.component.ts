import { Component, OnInit } from '@angular/core';
import {FormGroup} from "@angular/forms";
import {Field} from "../../model/field";

@Component({
  selector: 'app-input-control',
  templateUrl: './input-control.component.html',
  styleUrls: ['./input-control.component.scss']
})
export class InputControlComponent implements OnInit {
  group: FormGroup;
  field: any;
  constructor() { }

  ngOnInit() {
  }

}
