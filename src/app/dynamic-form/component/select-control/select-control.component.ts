import { Component, OnInit } from '@angular/core';
import {Field} from "../../model/field";
import {FormGroup} from "@angular/forms";

@Component({
  selector: 'app-select-control',
  templateUrl: './select-control.component.html',
  styleUrls: ['./select-control.component.scss']
})
export class SelectControlComponent implements OnInit {
  field:any;
  group:FormGroup;
  constructor() { }

  ngOnInit() {
  }

}
