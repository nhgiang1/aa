import {Directive, ComponentFactoryResolver, OnInit, ViewContainerRef, Input} from '@angular/core';
import {Field} from './model/field';
import {FormGroup} from "@angular/forms";
import {DateControlComponent} from './component/date-control/date-control.component';
import {InputControlComponent} from './component/input-control/input-control.component';
import {SelectControlComponent} from './component/select-control/select-control.component';

const component = {
  date: DateControlComponent,
  input: InputControlComponent,
  select: SelectControlComponent,
};

@Directive({
  selector: '[appDynamicForm]'
})
export class DynamicFormDirective implements OnInit {
  @Input() field: Field;
  @Input() group: FormGroup;
  componentRef: any;

  constructor(private resolver: ComponentFactoryResolver, private container: ViewContainerRef) {
  }

  ngOnInit() {
   const factory = this.resolver.resolveComponentFactory(component[this.field.type]);
   this.componentRef = this.container.createComponent(factory);
   this.componentRef.instance.field = this.field;
   this.componentRef.instance.group = this.group;
  }
}
